name 'lb'
maintainer 'Andrey'
maintainer_email 'you@example.com'
license 'All Rights Reserved'
description 'Installs/Configures Load Balancer'
long_description 'Installs/Configures Load Balancer'
version '0.0.2'
chef_version '>= 13.0'

depends 'acme', '= 4.0.0'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/nginx/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/nginx'
