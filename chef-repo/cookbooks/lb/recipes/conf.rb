#
# Cookbook:: nginx
# Recipe:: conf
#
# Copyright:: 2019, The Authors, All Rights Reserved.

template "/etc/nginx/nginx.conf" do
  source "etc/nginx/nginx.conf.erb"
  mode 0644
  notifies :reload, 'service[nginx]'
end

template "/etc/nginx/conf.d/#{node['nginx']['cert']['fqdn']}.conf" do
  source "etc/nginx/conf.d/#{node['nginx']['cert']['fqdn']}.conf.erb"
  mode 0644
  notifies :reload, 'service[nginx]'
end

file "/etc/nginx/htpasswd" do
  content "admin:$apr1$fFh4BSSX$ZAEjbZdrZYY5Gl9xm7xnz/"
  user 'www-data'
  mode 0640
end
