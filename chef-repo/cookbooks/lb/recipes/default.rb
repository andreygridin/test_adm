#
# Cookbook:: nginx
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

service 'nginx' do
  action :nothing
  supports :status => true, :restart => true, :reload => true
end

include_recipe 'lb::packages'
include_recipe 'lb::acme'
include_recipe 'lb::conf'
include_recipe 'lb::iptables'
