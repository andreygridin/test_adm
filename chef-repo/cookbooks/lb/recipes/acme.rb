#
# Cookbook:: nginx
# Recipe:: acme
#
# Copyright:: 2019, The Authors, All Rights Reserved.

include_recipe 'acme'

acme_selfsigned node['nginx']['cert']['fqdn'] do
  crt     "/etc/ssl/#{node['nginx']['cert']['fqdn']}.crt"
  chain   "/etc/ssl/#{node['nginx']['cert']['fqdn']}-chain.crt"
  key     "/etc/ssl/#{node['nginx']['cert']['fqdn']}.key"
end
