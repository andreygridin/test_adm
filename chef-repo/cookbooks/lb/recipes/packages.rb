#
# Cookbook:: nginx
# Recipe:: packages
#
# Copyright:: 2019, The Authors, All Rights Reserved.

apt_preference 'nginx' do
  pin 'origin nginx.org'
  pin_priority '900'
end

file "/etc/apt/sources.list.d/nginx.list" do
  content "deb http://nginx.org/packages/ubuntu #{node['lsb']['codename']} nginx"
end

execute "curl -fsSL https://nginx.org/keys/nginx_signing.key | sudo apt-key add -"

execute 'apt-get update'

[ 'gnupg2', 'ca-certificates', 'lsb-release' ].each do |pkg|
  package pkg
end

package 'nginx' do
  action :upgrade
  notifies :restart, 'service[nginx]'
end
