#
# Cookbook:: consul
# Recipe:: iptables
#
# Copyright:: 2019, The Authors, All Rights Reserved.

execute "/etc/network/if-up.d/iptables" do
  action :nothing
end

template "/etc/network/if-up.d/iptables" do
   source "etc/network/if-up.d/iptables.rb"
   mode 0755
   notifies :run, 'execute[/etc/network/if-up.d/iptables]'
end
