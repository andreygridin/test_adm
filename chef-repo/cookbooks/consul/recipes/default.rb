#
# Cookbook:: consul
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

package "wget"
package "unzip"

execute "Download and Install Consul" do
  command <<-EOF
  wget https://releases.hashicorp.com/consul/#{node['consul']['version']}/consul_#{node['consul']['version']}_linux_amd64.zip && \
  unzip consul_#{node['consul']['version']}_linux_amd64.zip && \
  mv consul /usr/local/bin/
  EOF
  not_if { File.exists?('/usr/local/bin/consul') }
end

include_recipe "consul::conf"
include_recipe "consul::iptables"
