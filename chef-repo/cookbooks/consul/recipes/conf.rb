#
# Cookbook:: consul
# Recipe:: conf
#
# Copyright:: 2019, The Authors, All Rights Reserved.

IP_ADDRESS = node[:network][:interfaces][:eth1][:addresses].detect{|k,v| v[:family] == "inet" }.first
SERVER_MODE = false
SERVER_MODE = true if node['consul']['mode'] == 'server'
UI = SERVER_MODE
BOOTSTRAP_EXPECT = 0
BOOTSTRAP_EXPECT = 3 if node['consul']['mode'] == 'server'

group 'consul' do
  system true
end

user 'consul' do
  group 'consul'
  shell '/sbin/nologin'
  system true
end

directory '/var/lib/consul' do
  owner 'consul'
  group 'consul'
  mode '755'
  recursive true
end

directory '/etc/consul.d' do
  owner 'consul'
  group 'consul'
end

systemd_unit 'consul.service' do
  content({Unit: {
            Description: 'Consul Service Discovery Agent',
            Documentation: ['https://www.consul.io/'],
            After: 'network-online.target',
            Wants: 'network-online.target'
          },
          Service: {
            Type: 'simple',
            User: 'consul',
            Group: 'consul',
            ExecStart: "/usr/local/bin/consul agent -node #{node.name} -config-dir=/etc/consul.d",
            ExecReload: '/bin/kill -HUP $MAINPID',
            KillSignal: 'SIGINT',
            TimeoutStopSec: 5,
            SyslogIdentifier: 'consul',
            Restart: 'on-failure'
          },
          Install: {
            WantedBy: 'multi-user.target',
          }})
  action :create
end

template '/etc/consul.d/config.json' do
  source 'etc/consul.d/config.json.erb'
  mode 0644
  variables(
    ip_address: IP_ADDRESS,
    ui: UI,
    server: SERVER_MODE,
    bootstrap_expect: BOOTSTRAP_EXPECT 
  )
end

service 'consul.service' do
  action [ :restart, :enable ]
end
