#!/bin/sh
#
# /etc/network/if-up.d/iptables
#

IPTABLES="/sbin/iptables"

echo "Starting firewall"

$IPTABLES -F -t nat
$IPTABLES -X -t nat


$IPTABLES -F
$IPTABLES -X

# Rules to avoid dos attacks on ssh
$IPTABLES -N SSH
$IPTABLES -A INPUT -p tcp --syn --dport 22 -j SSH
$IPTABLES -A SSH -p tcp -m tcp -s 127.0.0.0/8 --dport 22 -j ACCEPT
$IPTABLES -A SSH -p tcp -m tcp -s 10.0.0.0/8 --dport 22 -j ACCEPT

$IPTABLES -A SSH -p tcp --syn --dport 22 -m recent --name SSH --set
$IPTABLES -A SSH -p tcp --syn --dport 22 -m recent --name SSH --update --seconds 60 --hitcount 2 -j DROP
$IPTABLES -A INPUT -m state --state NEW -m tcp -p tcp --dport 1159 -m recent --name SSH --remove -j DROP

$IPTABLES -P INPUT DROP
$IPTABLES -A INPUT -i lo -j ACCEPT
$IPTABLES -A INPUT -p icmp --icmp-type 8 -j ACCEPT
$IPTABLES -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# allow lb to UI
$IPTABLES -A INPUT -p tcp --match multiport --dports 8500 -s 10.0.1.0/24 -j ACCEPT
# allow serf, consul agent/server
$IPTABLES -A INPUT -p tcp --match multiport --dports 8300,8301,8302 -s 10.0.1.0/24 -j ACCEPT

<% if node['consul']['mode'] == 'agent' %>
# allow redis
$IPTABLES -A INPUT -p tcp -m tcp --dport 6379 -s 10.0.1.0/24 -j ACCEPT
<% end %>
