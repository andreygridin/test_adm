#!/usr/bin/env python

import consulate, re, socket

def check_node(agent):
   checks = session.health.state('critical')
   for check in checks:
      if re.search(agent, check['Node']):
         return False
   return True

def service_discovery():
   failed_checks = session.health.state('critical')
   if len(session.agent.services()[0].keys()) == 0 or len(failed_checks):
      if re.search('agent0', str(local_hostname)):
         tags = [ 'pri' ]
      elif re.search('agent1', str(local_hostname)) and check_node('agent0') is False:
         tags = [ 'pri' ]
      else:
         tags = [ 'sec' ]
      session.agent.service.register('redis', port=6379, tags=tags)
      print "Made redis service registration for agent %s with tag %s" % (local_hostname, tags[0])

def main():
   service_discovery()

if __name__ == "__main__":
   local_hostname = socket.gethostname()
   session = consulate.Consul()
   main()
