#
# Cookbook:: redis
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

package "python"
package "python-pip"

execute "pip install consulate"

cookbook_file "/usr/local/bin/redis_service_discovery.py" do
  source "usr/local/bin/redis_service_discovery.py"
  mode 0755
end

package "redis-server"

execute "Make service discovery for Redis" do
  command "/usr/local/bin/redis_service_discovery.py"
  notifies :restart, 'service[consul.service]'
end

cron_d "redis_service_discovery" do
  minute '*/1'
  user 'redis'
  command "/usr/local/bin/redis_service_discovery.py"
end
