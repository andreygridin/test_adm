#
# Cookbook:: lb-keepalived
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

include_recipe "keepalived::default"

keepalived_global_defs 'global_defs' do
  notification_email %w( noc@example.com )
  notification_email_from "keepalived@#{node.name}"
  router_id node.name
  enable_traps true
end

keepalived_vrrp_instance "#{node.name}" do
  master node['keepalived']['master']
  interface 'eth1'
  virtual_router_id node['keepalived']['router_id']
  priority node['keepalived']['priority']
  authentication auth_type: 'PASS', auth_pass: '123456789'
  virtual_ipaddress node['keepalived']['virtual_ipaddress']
end
