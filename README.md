# test adm 

## Системные требования
Для запуска потребуется ~3,5 Gb ОЗУ и ~12 Gb диска. Тестировалось на MacOS X.

## Версии инструментов

```
$ sw_vers
ProductName:	Mac OS X
ProductVersion:	10.14.3
BuildVersion:	18D109

$ chef --version
Chef Development Kit Version: 3.6.57
chef-client version: 14.8.12
delivery version: master (5fb4908da53579c9dcf4894d4acf94d2d9ee6475)
berks version: 7.0.7
kitchen version: 1.24.0
inspec version: 3.2.6

$ vagrant --version
Vagrant 2.2.4
$ VBoxManage --version
6.0.4r128413
```

## Сборка окружения
```
git clone https://bitbucket.org/andreygridin/test_adm
cd ./test_adm
kitchen converge
```

## Запуск тестов
```
kitchen verify
```

## Ссылка на Consul UI

[Consul UI](http://10.0.0.2/)

Либо прописываем в /etc/hosts 
```
10.0.0.2 test-consul-webui.local
```
10.0.0.2 можно поменять на 10.0.0.3 -- они равнозначны, keepalived. На FQDN test-consul-webui.local сгенерирован selfsigned сертификат.

## Redis service discovery
Добавление имен осуществляется небольшим python скриптом, который выполняется на consul-agent нодах по cron раз в минуту. Так мы управляем суффиксом "pri" (от primary) для сервиса "redis". Иначе говоря, в случае падения одной из нод redis, сервисное pri.redis будет резолвиться по-разному.

