# # encoding: utf-8

# Inspec test for recipe lb

describe service('keepalived') do
  it { should be_enabled }
  it { should be_running }
end
