# # encoding: utf-8

# Inspec test for recipe lb

describe port(80) do
  it { should be_listening }
end

describe port(443) do
  it { should be_listening }
end

describe nginx do
  its('version') { should cmp >= '1.14.2' }
end

describe nginx do
  its('support_info') { should match /TLS/ }
end

describe service('nginx') do
  it { should be_enabled }
  it { should be_running }
end
