# # encoding: utf-8

# Inspec test for recipe consul-agent

describe port('127.0.0.1', 6379) do
  it { should be_listening }
end

describe service('redis') do
  it { should be_enabled }
  it { should be_running }
end
