# # encoding: utf-8

# Inspec test for recipe consul-server

describe port(22) do
  it { should be_listening }
end

describe service('ssh') do
  it { should be_enabled }
  it { should be_running }
end
