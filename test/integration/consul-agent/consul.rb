# # encoding: utf-8

# Inspec test for recipe consul-agent

describe port(8600) do
  it { should be_listening }
end

describe port(8500) do
  it { should be_listening }
end

describe port(8301) do
  it { should be_listening }
  its('protocols') { should include('tcp') }
  its('protocols') { should include('udp') }
end

describe service('consul') do
  it { should be_enabled }
  it { should be_running }
end
